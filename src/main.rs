// Data tape
// Data pointer
// Instruction list (the program)
// Instruction pointer
// > Move data pointer right
// < Move data pointer left
// + Increment current data field
// - Decrement current data field
// . Print current data field (converted to ascii)
// , Read single character and put into data field
// [ If the current data field is zero skip to next ]
// ] If the current field is non-zero go back to [
mod parser;
mod program;
mod token;
use crate::program::*;

fn main() {
    let code = "++++++++++[>++++++++++<-]>---.+.+.+.+.+.+.+.+.";
    let program = Program::compile(code);
    run(program);
    println!("\nProgram finished!");
}
